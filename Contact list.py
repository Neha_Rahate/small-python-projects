import copy
Info = []
details = []
def getdata():
    details.clear()
    Name = input("Enter your Name here: ")
    details.append(Name)
    Mob_no = int(input("Enter your Mobile Number: "))
    if len(str(Mob_no)) < 10 or len(str(Mob_no)) > 10:
          raise ValueError("Invalid Mobile number.")
    details.append(Mob_no)
    Email = input("Enter your Email Id: ")
    if "@gmail.com" not in Email:
        raise ValueError("Invalid Email Address .")
    details.append(Email)
    Company = input("Enter your Company Name: ")
    details.append(Company)
    Address = input("Enter your Address: ")
    details.append(Address)
    Info.append(copy.deepcopy(details))
    print("Contact added Successfully..!")

def displaydata():
    for i in Info:
        print(i)

def search_data():
    user_input = input("Enter the Contact Name: ")
    for i in Info:
        if i[0] == user_input:
            print(i)
            break
    else:
         print("Name you entered is not found in Contact Book..!")

def edit_contact():
    C_Ed = input("Enter the contact name to be edited: ")
    for i in Info:
        if i[0] == C_Ed:
            p = input("Do you really want to edit contact? type yes for Yes & no for No:")
            if p == 'yes':
                print("What do you want to edited?")
                x = int(input(" 1. Name \n 2. Mobile_No \n 3. Email \n 4. Company Name \n 5. Address \n Enetr your Choice: "))
                if x == 1:
                    y = input("Enter new name:")
                    i[0] = y
                    print("Contact Edited Successfully..!")
                elif x == 2:
                    y = int(input("Enter new Mobile No.:"))
                    i[1] = y
                    print("Contact Edited Successfully..!")
                elif x == 3:
                    y = input("Enter new Email:")
                    i[2] = y
                    print("Contact Edited Successfully..!")
                elif x == 4:
                    y = input("Enter new Company Name:")
                    i[3] = y
                    print("Contact Edited Successfully..!")
                elif x == 5:
                    y = input("Enter new Address:")
                    i[4] = y
                    print("Contact Edited Successfully..!")
                else:
                    print("Please enter the correct input..!")
                break
                print("Contact Edited Successfully..!")
            else:
                print("Incorrect Input !")
        else:
            print("Name is not found in the Contact Book..!")

def del_contact():
    C_del = input("Enter contact Name to be delete:")
    for i in Info:
        if i[0] == C_del:
            Info.remove(i)
            print("Contact deleted Successfully..!")
            break
    else:
         print("Name is not found in the Contact Book..!")

print(" 1. Add New Contact \n 2. Search Contact \n 3. Display Contact \n 4. Edit Contact \n 5. Delete Contact \n 6. Exit ")
while True:
    choice = int(input("Enter your Choice: "))
    if choice == 1:
        getdata()

    elif choice == 2:
        search_data()

    elif choice == 3:
        if not Info:
            print("Empty Contact Book..!")
        else:
            displaydata()

    elif choice == 4:
        edit_contact()

    elif choice == 5:
        del_contact()

    elif choice == 6:
        break

    else:
        print("You enter invalid input..!")